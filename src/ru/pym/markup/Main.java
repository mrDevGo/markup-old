package ru.pym.markup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.io.IOException;


public class Main extends Activity {
    private final String SETTINGS = "settings";
    public static SharedPreferences preferences;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        loadSettings(preferences);
        finish();
        startActivity(new Intent(this, ActivityCalcPriceOfGoods.class));
    }

    private void loadSettings(SharedPreferences preferences) {
        try {
            Settings.a1_primeCoast = preferences.getString("a1_primeCoast", "");
            Settings.a1_markup = preferences.getString("a1_markup", "");
            Settings.a2_markup = preferences.getString("a2_markup", "");
            Settings.a2_priceOfGoods = preferences.getString("a2_priceOfGoods", "");

            Settings.a3_lease = preferences.getString("a3_lease", "");
            Settings.a3_markup = preferences.getString("a3_markup", "");
            Settings.a3_overall = preferences.getString("a3_overall", "");
            Settings.a3_salary = preferences.getString("a3_salary", "");
            Settings.a3_saleProceeds = preferences.getString("a3_saleProceeds", "");
            Settings.a3_tallage = preferences.getString("a3_tallage", "");

            Settings.a4_priceOfGoods = preferences.getString("a4_discountSize", "");
            Settings.a4_discount = preferences.getString("a4_discount", "");
        }catch (Exception e){
            throw new RuntimeException("Не возможно загрузить настройки");
        }

    }
}
