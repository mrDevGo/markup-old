package ru.pym.markup;
import android.content.SharedPreferences;

public class Settings {

    public static String a1_markup="";
    public static String a1_primeCoast="";
    public static String a2_priceOfGoods="";
    public static String a2_markup="";

    public static String a3_saleProceeds="";
    public static String a3_salary="";
    public static String a3_tallage="";
    public static String a3_lease="";
    public static String a3_overall="";
    public static String a3_markup="";

    public static String a4_priceOfGoods= "";
    public static String a4_discount= "";



    public static void saveSettings(SharedPreferences sharedPreferences, int numberActivity) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (numberActivity==1){
            editor.clear();
            editor.putString("a1_markup",a1_markup );
            editor.putString("a1_primeCoast",a1_primeCoast );
            editor.apply();
        }
        if (numberActivity==2){
            editor.clear();
            editor.putString("a2_priceOfGoods",a2_priceOfGoods );
            editor.putString("a2_markup",a2_markup );
            editor.apply();
        }
        if (numberActivity==3){
            editor.clear();
            editor.putString("a3_lease",a3_lease);
            editor.putString("a3_markup",a3_markup);
            editor.putString("a3_overall",a3_overall);
            editor.putString("a3_salary",a3_salary);
            editor.putString("a3_saleProceeds",a3_saleProceeds);
            editor.putString("a3_tallage",a3_tallage);
            editor.apply();
        }
        if (numberActivity==4){
            editor.clear();
            editor.putString("a4_discount",a4_discount);
            editor.putString("a4_priceOfGoods",a4_priceOfGoods);
            editor.apply();
        }

    }

}