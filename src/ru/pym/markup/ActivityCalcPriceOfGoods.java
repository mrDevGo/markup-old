package ru.pym.markup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class ActivityCalcPriceOfGoods extends Activity {

    private EditText et_primeCoast;
    private EditText et_markup;
    private TextView tv_priceGoods;
    private TextView tv_primeCoast2;
    private TextView tv_markup2;
    private TextView tv_priceOfGoods2;
    private TextView tv_enterMarkup;
    private TextView tv_enterPrimeCoast;

    private double result = 0;
    private double primeCoast = 0;
    private double markup = 0;
    private double percent = 0;

    private boolean focusable;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc_price_of_goods_a1);
        defineObjects();
        et_primeCoast.requestFocus();
        et_primeCoast.addTextChangedListener(et_primeCoastWatcher);
        et_markup.addTextChangedListener(et_MarkupWatcher);
        et_primeCoast.setText(Settings.a1_primeCoast);
        et_markup.setText(Settings.a1_markup);

        if (et_primeCoast.getText().equals("")|| et_markup.getText().equals("")){
            if (et_primeCoast.getText().equals("")){
                tv_enterPrimeCoast.setVisibility(View.VISIBLE);
                tv_enterMarkup.setVisibility(View.INVISIBLE);
            }
            if (et_markup.getText().equals("")){
                tv_enterPrimeCoast.setVisibility(View.INVISIBLE);
                tv_enterMarkup.setVisibility(View.VISIBLE);
            }

        }


        focusable=true;
    }

    private void defineObjects() {
        et_primeCoast = (EditText) findViewById(R.id.a4_et_priceOfGoods);
        et_markup = (EditText) findViewById(R.id.a4_et_discount);
        tv_priceGoods = (TextView) findViewById(R.id.a4_tv_discountPrice);
        tv_primeCoast2 = (TextView) findViewById(R.id.a1_lbl_PrimeCoast2);
        tv_markup2 = (TextView) findViewById(R.id.a1_lbl_markup2);
        tv_priceOfGoods2 = (TextView) findViewById(R.id.a1_lbl_priceOfGoods2);
        tv_enterMarkup = (TextView) findViewById(R.id.a1_tv_enterMarkup);
        tv_enterPrimeCoast = (TextView) findViewById(R.id.a4_tv_enterPrice);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calc_price_of_goods, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_calcPrimeCoast:
                finish();
                startActivity(new Intent(this, ActivityCalcPrimeCoast.class));
                break;
            case R.id.menu_calcSaleProceeds:
                finish();
                startActivity(new Intent(this, ActivityCalcSaleProceeds.class));
                break;
            case R.id.menu_help:
                startActivity(new Intent(this, ActivityHelp.class));
                break;
            case R.id.menu_about:
                startActivity(new Intent(this, ActivityContacts.class));
                break;
            case R.id.menu_calcDiscount:
                finish();
                startActivity(new Intent(this, ActivityCalcDiscount.class));
                break;
            case R.id.a1_btn_clear_all:
                et_markup.setText("");
                tv_priceGoods.setText("");
                et_primeCoast.setText("");
                tv_enterMarkup.setVisibility(View.INVISIBLE);
                tv_enterPrimeCoast.setVisibility(View.VISIBLE);
                break;
            case R.id.a1_btn_save:
                try {
                    Settings.a1_markup= et_markup.getText().toString();
                    Settings.a1_primeCoast=et_primeCoast.getText().toString();
                        Settings.saveSettings(Main.preferences,1);
                        showToastMessage("Параметры сохранены");
                }catch (Exception e){
            showToastMessage("Не возможно сохранить параметры");
                }
                break;
            default:
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private TextWatcher et_primeCoastWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updatePriceofGoods();
        }
        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher et_MarkupWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updatePriceofGoods();
        }
        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public void buttonClick(View view) {

        switch (view.getId()) {
            case R.id.a4_btnClear:
                if (et_primeCoast==getCurrentFocus()){
                    et_primeCoast.setText("");
                    tv_enterMarkup.setVisibility(View.INVISIBLE);
                    tv_enterPrimeCoast.setVisibility(View.VISIBLE);
                }
                if (et_markup ==getCurrentFocus()){
                    et_markup.setText("");
                    tv_enterMarkup.setVisibility(View.VISIBLE);
                    tv_enterPrimeCoast.setVisibility(View.INVISIBLE);
                }

                break;
            case R.id.a4_btnEnter:

                if (et_markup ==getCurrentFocus()&&focusable){
                    et_primeCoast.requestFocus();
                    focusable=false;
                }
                if (et_primeCoast==getCurrentFocus()&&focusable){
                    et_markup.requestFocus();
                    focusable=false;
                }
                break;

            case R.id.a1_btnPoint:
                if (et_primeCoast==getCurrentFocus()){
                    et_primeCoast.setText(et_primeCoast.getText()+".");
                }
                if (et_markup ==getCurrentFocus()){
                    et_markup.setText(et_markup.getText()+".");
                }
                break;
            default:{
                if (et_primeCoast==getCurrentFocus()){
                    et_primeCoast.setText(et_primeCoast.getText()+view.getContentDescription().toString());
                    focusable=true;
                }
                if (et_markup ==getCurrentFocus()){
                    et_markup.setText(et_markup.getText()+view.getContentDescription().toString());
                    focusable=true;
                }
            }
            break;
        }

    }

    private void updatePriceofGoods() {

        if (et_markup.getText().toString().equals("")) {
            tv_priceGoods.setText("");
            tv_enterPrimeCoast.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.VISIBLE);
        } else if (et_primeCoast.getText().toString().equals("")) {
            tv_priceGoods.setText("");
            tv_enterMarkup.setVisibility(View.INVISIBLE);
            tv_enterPrimeCoast.setVisibility(View.VISIBLE);
        }


        try {
            markup = Double.parseDouble(et_markup.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            primeCoast = Double.parseDouble(et_primeCoast.getText().toString());
        } catch (Exception e) {
            return;
        }

        percent = (primeCoast * markup) / 100;
        result = primeCoast + percent;

        if (result % 1 == 0) {
            tv_enterPrimeCoast.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
            tv_priceGoods.setText("" + (int) result);
        } else {
            tv_enterPrimeCoast.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
            tv_priceGoods.setText(String.format(Locale.US, "%.2f", result));
        }

    }

    private void showToastMessage(String message) {
        Toast toast=Toast.makeText(this,message,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM,0,0);
        toast.show();
    }

}