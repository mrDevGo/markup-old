package ru.pym.markup;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class ActivityCalcPrimeCoast extends Activity {

    private EditText et_priceGoods;
    private EditText et_markup;
    private TextView tv_primeCoast;
    private TextView tv_enterMarkup;
    private TextView tv_enterPriceOfGoods;


    private double result = 0;
    private double priceOfGoods = 0;
    private double markup = 0;
    private double percent = 0;

    private boolean focusable;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc_prime_coast_a2);
        defineObjects();
        et_priceGoods.requestFocus();
        et_priceGoods.addTextChangedListener(et_PriceGoodsWatcher);
        et_markup.addTextChangedListener(et_MarkupWatcher);
        et_priceGoods.setText(Settings.a2_priceOfGoods);
        et_markup.setText(Settings.a2_markup);
        et_priceGoods.getBackground().setColorFilter(Color.argb(255, 0, 255, 0), PorterDuff.Mode.CLEAR);
        et_markup.getBackground().setColorFilter(Color.argb(255, 0, 255, 0), PorterDuff.Mode.CLEAR);

        if (et_priceGoods.getText().equals("")||et_markup.getText().equals("")) {
            if (et_priceGoods.getText().equals("")) {
                tv_enterPriceOfGoods.setVisibility(View.VISIBLE);
                tv_enterMarkup.setVisibility(View.INVISIBLE);
            }
            if (et_markup.getText().equals("")){
                tv_enterPriceOfGoods.setVisibility(View.INVISIBLE);
                tv_enterMarkup.setVisibility(View.VISIBLE);
            }

        }


    }

    private void defineObjects() {
        et_priceGoods = (EditText) findViewById(R.id.a2_et_priceOfGoods);
        et_markup = (EditText) findViewById(R.id.a2_et_Markup);
        tv_primeCoast = (TextView) findViewById(R.id.a2_tv_primeCoast);
        tv_enterMarkup = (TextView) findViewById(R.id.a2_tv_enterMarkup);
        tv_enterPriceOfGoods = (TextView) findViewById(R.id.a2_tv_enterPriceOfGoods);
        focusable = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calc_prime_coast, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_CalcPriceOfGoods:
                finish();
                startActivity(new Intent(this, ActivityCalcPriceOfGoods.class));
                break;
            case R.id.menu_calcSaleProceeds:
                finish();
                startActivity(new Intent(this, ActivityCalcSaleProceeds.class));
                break;
            case R.id.menu_help:
                startActivity(new Intent(this, ActivityHelp.class));
                break;
            case R.id.menu_about:
                startActivity(new Intent(this, ActivityContacts.class));
                break;
            case R.id.menu_calcDiscount:
                finish();
                startActivity(new Intent(this, ActivityCalcDiscount.class));
                break;
            case R.id.a2_btn_clear_all:
                et_priceGoods.setFocusable(true);
                tv_primeCoast.setText("");
                et_priceGoods.setText("");
                et_markup.setText("");
                tv_enterMarkup.setVisibility(View.INVISIBLE);
                tv_enterPriceOfGoods.setVisibility(View.VISIBLE);
                break;
            case R.id.a2_btn_save:
                try {
                    Settings.a2_markup = et_markup.getText().toString();
                    Settings.a2_priceOfGoods = et_priceGoods.getText().toString();
                    Settings.saveSettings(Main.preferences, 1);
                    showToastMessage("Параметры сохранены");
                } catch (Exception e) {
                    showToastMessage("Не возможно сохранить параметры");
                }
                break;
            default:
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private TextWatcher et_PriceGoodsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updatePriceofGoods();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher et_MarkupWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updatePriceofGoods();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private void updatePriceofGoods() {
        if (et_markup.getText().toString().equals("")) {
            tv_primeCoast.setText("");
            tv_enterPriceOfGoods.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.VISIBLE);
        } else if (et_priceGoods.getText().toString().equals("")) {
            tv_primeCoast.setText("");
            tv_enterMarkup.setVisibility(View.INVISIBLE);
            tv_enterPriceOfGoods.setVisibility(View.VISIBLE);
        }

        try {
            markup = Double.parseDouble(et_markup.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            priceOfGoods = Double.parseDouble(et_priceGoods.getText().toString());
        } catch (Exception e) {
            return;
        }

        percent = (markup / 100) + 1;
        result = priceOfGoods / percent;

        if (result % 1 == 0) {
            tv_enterPriceOfGoods.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
            tv_primeCoast.setText("" + (int) result);
        } else {
            tv_enterPriceOfGoods.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
            tv_primeCoast.setText(String.format(Locale.US, "%.2f", result));
        }

    }

    public void buttonClick(View view) {

        switch (view.getId()) {
            case R.id.a2_btnClear:
                if (et_priceGoods == getCurrentFocus()) {
                    et_priceGoods.setText("");
                    tv_enterPriceOfGoods.setVisibility(View.VISIBLE);
                    tv_enterMarkup.setVisibility(View.INVISIBLE);
                }
                if (et_markup == getCurrentFocus()) {
                    et_markup.setText("");
                    tv_enterPriceOfGoods.setVisibility(View.INVISIBLE);
                    tv_enterMarkup.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.a2_btnEnter:

                if (et_markup == getCurrentFocus() && focusable) {
                    et_priceGoods.requestFocus();
                    focusable = false;
                }
                if (et_priceGoods == getCurrentFocus() && focusable) {
                    et_markup.requestFocus();
                    focusable = false;
                }
                break;

            case R.id.a2_btnPoint:
                if (et_priceGoods == getCurrentFocus()) {
                    et_priceGoods.setText(et_priceGoods.getText() + ".");
                }
                if (et_markup == getCurrentFocus()) {
                    et_markup.setText(et_markup.getText() + ".");
                }
                break;
            default: {
                if (et_priceGoods == getCurrentFocus()) {
                    et_priceGoods.setText(et_priceGoods.getText() + view.getContentDescription().toString());
                    focusable = true;
                }
                if (et_markup == getCurrentFocus()) {
                    et_markup.setText(et_markup.getText() + view.getContentDescription().toString());
                    focusable = true;
                }
            }
            break;
        }

    }

    private void showToastMessage(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }
}
