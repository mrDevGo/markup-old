package ru.pym.markup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class ActivityCalcDiscount extends Activity {
    /**/
    private EditText et_priceOfGoods;
    private EditText et_discount;
    private TextView tv_discountPrice;
    private TextView tv_discountSize;
    private TextView tv_enterDiscount;
    private TextView tv_enterPrice;

    private double priceOfGoods = 0;
    private double discount = 0;
    private double discountSize = 0;
    private double discountPrice = 0;

    private boolean focusable;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc_discount_a4);
        defineObjects();
        et_priceOfGoods.requestFocus();
        et_priceOfGoods.addTextChangedListener(et_priceOfGoodsWatcher);
        et_discount.addTextChangedListener(et_discountWatcher);
        et_discount.setText(Settings.a4_discount);
        et_priceOfGoods.setText(Settings.a4_priceOfGoods);

        if (et_priceOfGoods.getText().equals("")||et_discount.getText().equals("")) {
            if (et_priceOfGoods.getText().equals("")){
                tv_enterPrice.setVisibility(View.VISIBLE);
                tv_enterDiscount.setVisibility(View.INVISIBLE);
            }
            if (et_discount.getText().equals("")){
                tv_enterPrice.setVisibility(View.INVISIBLE);
                tv_enterDiscount.setVisibility(View.VISIBLE);
            }
        }
    }

    private void defineObjects() {
        et_priceOfGoods = (EditText) findViewById(R.id.a4_et_priceOfGoods);
        et_discount = (EditText) findViewById(R.id.a4_et_discount);
        tv_discountPrice = (TextView) findViewById(R.id.a4_tv_discountPrice);
        tv_discountSize = (TextView) findViewById(R.id.a4_tv_discountSize);
        tv_enterDiscount = (TextView) findViewById(R.id.a4_tv_enterDiscount);
        tv_enterPrice = (TextView) findViewById(R.id.a4_tv_enterPrice);
        focusable = true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calc_discount, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_CalcPriceOfGoods:
                finish();
                startActivity(new Intent(this, ActivityCalcPriceOfGoods.class));
                break;
            case R.id.menu_calcPrimeCoast:
                finish();
                startActivity(new Intent(this, ActivityCalcPrimeCoast.class));
                break;
            case R.id.menu_calcSaleProceeds:
                finish();
                startActivity(new Intent(this, ActivityCalcSaleProceeds.class));
                break;
            case R.id.menu_help:
                startActivity(new Intent(this, ActivityHelp.class));
                break;
            case R.id.menu_about:
                startActivity(new Intent(this, ActivityContacts.class));
                break;
            case R.id.a4_btn_clear_all:
                et_priceOfGoods.setText("");
                et_discount.setText("");
                tv_discountSize.setText("");
                tv_enterDiscount.setVisibility(View.INVISIBLE);
                tv_enterPrice.setVisibility(View.VISIBLE);
                break;
            case R.id.a4_btn_save:
                try {
                    Settings.a4_discount = et_discount.getText().toString();
                    Settings.a4_priceOfGoods = et_priceOfGoods.getText().toString();
                    Settings.saveSettings(Main.preferences, 4);
                    showToastMessage("Парметры сохранены");

                } catch (Exception e) {
                    showToastMessage("Не возможно сохранить параметры");
                }

                break;

            default:
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void showToastMessage(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    private TextWatcher et_discountWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateDiscountPrice();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher et_priceOfGoodsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateDiscountPrice();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public void buttonClick(View view) {

        switch (view.getId()) {
            case R.id.a4_btnClear:
                if (et_priceOfGoods == getCurrentFocus()) {
                    et_priceOfGoods.setText("");
                    tv_enterDiscount.setVisibility(View.INVISIBLE);
                    tv_enterPrice.setVisibility(View.VISIBLE);
                }
                if (et_discount == getCurrentFocus()) {
                    et_discount.setText("");
                    tv_enterDiscount.setVisibility(View.VISIBLE);
                    tv_enterPrice.setVisibility(View.INVISIBLE);
                }


                break;
            case R.id.a4_btnEnter:

                if (et_priceOfGoods == getCurrentFocus() && focusable) {
                    et_discount.requestFocus();
                    focusable = false;
                }
                if (et_discount == getCurrentFocus() && focusable) {
                    et_priceOfGoods.requestFocus();
                    focusable = false;
                }
                break;

            case R.id.a1_btnPoint:
                if (et_priceOfGoods == getCurrentFocus()) {
                    et_priceOfGoods.setText(et_priceOfGoods.getText() + ".");
                }
                if (et_discount == getCurrentFocus()) {
                    et_discount.setText(et_discount.getText() + ".");
                }
                break;
            default: {
                if (et_priceOfGoods == getCurrentFocus()) {
                    et_priceOfGoods.setText(et_priceOfGoods.getText() + view.getContentDescription().toString());
                    focusable = true;
                }
                if (et_discount == getCurrentFocus()) {
                    et_discount.setText(et_discount.getText() + view.getContentDescription().toString());
                    focusable = true;
                }
            }
            break;
        }

    }

    private void updateDiscountPrice() {

        if (et_discount.getText().toString().equals("")) {
            tv_discountPrice.setText("");
            tv_discountSize.setText("");
            tv_enterDiscount.setVisibility(View.VISIBLE);
            tv_enterPrice.setVisibility(View.INVISIBLE);
        } else if (et_priceOfGoods.getText().toString().equals("")) {
            tv_discountPrice.setText("");
            tv_discountSize.setText("");
            tv_enterDiscount.setVisibility(View.INVISIBLE);
            tv_enterPrice.setVisibility(View.VISIBLE);
        }

        try {
            discount = Double.parseDouble(et_discount.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            priceOfGoods = Double.parseDouble(et_priceOfGoods.getText().toString());
        } catch (Exception e) {
            return;
        }

        discountSize = (priceOfGoods * discount) / 100;

        discountPrice = priceOfGoods - discountSize;

        if (discountPrice % 1 == 0) {
            tv_enterPrice.setVisibility(View.INVISIBLE);
            tv_enterDiscount.setVisibility(View.INVISIBLE);
            tv_discountPrice.setText("" + (int) discountPrice);
        } else {
            tv_enterDiscount.setVisibility(View.INVISIBLE);
            tv_enterPrice.setVisibility(View.INVISIBLE);
            tv_discountPrice.setText(String.format(Locale.US, "%.2f", discountPrice));
        }

        if (discountSize % 1 == 0) {
            tv_enterPrice.setVisibility(View.INVISIBLE);
            tv_enterDiscount.setVisibility(View.INVISIBLE);
            tv_discountSize.setText("" + (int) discountSize);
        } else {
            tv_enterDiscount.setVisibility(View.INVISIBLE);
            tv_enterPrice.setVisibility(View.INVISIBLE);
            tv_discountSize.setText(String.format(Locale.US, "%.2f", discountSize));
        }

    }

}