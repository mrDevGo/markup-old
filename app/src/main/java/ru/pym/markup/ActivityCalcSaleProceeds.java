package ru.pym.markup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityCalcSaleProceeds extends Activity {
    private TextView tv_netProceeds;
    private TextView tv_enterSaleProceeds;
    private TextView tv_enterSalary;
    private TextView tv_enterLease;
    private TextView tv_enterTallage;
    private TextView tv_enterOverall;
    private TextView tv_enterMarkup;


    private EditText et_saleProceeds;
    private EditText et_salary;
    private EditText et_lease;
    private EditText et_tallage;
    private EditText et_overall;
    private EditText et_markup;


    private double saleProceeds = 0;
    private double salary = 0;
    private double lease = 0;
    private double tallage = 0;
    private double overall = 0;
    private double markup = 0;
    private double percent = 0;
    private double temp = 0;
    private double result = 0;
    private boolean focusable;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc_sale_proceeds_a3);
        defineObjects();
        addListeners();
        et_saleProceeds.setText(Settings.a3_saleProceeds);
        et_lease.setText(Settings.a3_lease);
        et_tallage.setText(Settings.a3_tallage);
        et_overall.setText(Settings.a3_overall);
        et_salary.setText(Settings.a3_salary);
        et_markup.setText(Settings.a3_markup);

        if (et_saleProceeds.getText().equals("")){
            tv_enterSaleProceeds.setVisibility(View.VISIBLE);
        }
        tv_enterLease.setVisibility(View.INVISIBLE);
        tv_enterMarkup.setVisibility(View.INVISIBLE);
        tv_enterOverall.setVisibility(View.INVISIBLE);
        tv_enterSalary.setVisibility(View.INVISIBLE);
        tv_enterTallage.setVisibility(View.INVISIBLE);
    }

    private void addListeners() {
        et_saleProceeds.addTextChangedListener(et_saleProceedsWatcher);
        et_lease.addTextChangedListener(et_leaseWatcher);
        et_tallage.addTextChangedListener(et_tallageWatcher);
        et_overall.addTextChangedListener(et_overallWatcher);
        et_salary.addTextChangedListener(et_salaryWatcher);
        et_markup.addTextChangedListener(et_markupWatcher);
    }

    private void defineObjects() {
        tv_netProceeds = (TextView) findViewById(R.id.a3_tv_netProceeds);
        et_saleProceeds = (EditText) findViewById(R.id.a3_et_saleProceeds);
        et_lease = (EditText) findViewById(R.id.a3_et_lease);
        et_tallage = (EditText) findViewById(R.id.a3_et_tallage);
        et_overall = (EditText) findViewById(R.id.a3_et_overall);
        et_markup = (EditText) findViewById(R.id.a3_et_markup);
        et_salary = (EditText) findViewById(R.id.a3_et_salary);
        tv_enterSaleProceeds = (TextView) findViewById(R.id.a3_tv_enterSaleProceeds);
        tv_enterSalary = (TextView) findViewById(R.id.a3_tv_enterSalary);
        tv_enterLease = (TextView) findViewById(R.id.a3_tv_enterLease);
        tv_enterTallage = (TextView) findViewById(R.id.a3_tv_enterTallage);
        tv_enterOverall = (TextView) findViewById(R.id.a3_tv_enterOverall);
        tv_enterMarkup = (TextView) findViewById(R.id.a3_tv_enterMarkup);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calc_sale_proceeds, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_CalcPriceOfGoods:
                finish();
                startActivity(new Intent(this, ActivityCalcPriceOfGoods.class));
                break;
            case R.id.menu_calcPrimeCoast:
                finish();
                startActivity(new Intent(this, ActivityCalcPrimeCoast.class));
                break;
            case R.id.menu_help:
                startActivity(new Intent(this, ActivityHelp.class));
                break;
            case R.id.menu_about:
                startActivity(new Intent(this, ActivityContacts.class));
                break;
            case R.id.menu_calcDiscount:
                startActivity(new Intent(this, ActivityCalcDiscount.class));
                break;
            case R.id.a3_btn_clear_all:
                et_saleProceeds.setFocusable(true);
                et_saleProceeds.setText("");
                et_salary.setText("");
                et_lease.setText("");
                et_tallage.setText("");
                et_overall.setText("");
                et_markup.setText("");
                tv_netProceeds.setText("");
                tv_enterSaleProceeds.setVisibility(View.VISIBLE);
                tv_enterLease.setVisibility(View.INVISIBLE);
                tv_enterMarkup.setVisibility(View.INVISIBLE);
                tv_enterOverall.setVisibility(View.INVISIBLE);
                tv_enterSalary.setVisibility(View.INVISIBLE);
                tv_enterTallage.setVisibility(View.INVISIBLE);
                break;
            case R.id.a3_btn_save:
                try {
                    Settings.a3_markup=et_markup.getText().toString();
                    Settings.a3_tallage=et_tallage.getText().toString();
                    Settings.a3_lease=et_lease.getText().toString();
                    Settings.a3_saleProceeds=et_saleProceeds.getText().toString();
                    Settings.a3_salary=et_salary.getText().toString();
                    Settings.a3_overall=et_overall.getText().toString();

                    Settings.saveSettings(Main.preferences,1);
                    showToastMessage("Параметры сохранены");
                }catch (Exception e){
                    showToastMessage("Не возможно сохранить параметры");
                }
                break;
            default:
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void updateNetProceeds() {

        if (et_saleProceeds.getText().toString().equals("")) {
            tv_netProceeds.setText("");
            tv_enterSaleProceeds.setVisibility(View.VISIBLE);
            tv_enterSalary.setVisibility(View.INVISIBLE);
            tv_enterLease.setVisibility(View.INVISIBLE);
            tv_enterTallage.setVisibility(View.INVISIBLE);
            tv_enterOverall.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
        }else if (et_lease.getText().toString().equals("")) {
            tv_netProceeds.setText("");
            tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
            tv_enterSalary.setVisibility(View.INVISIBLE);
            tv_enterLease.setVisibility(View.VISIBLE);
            tv_enterTallage.setVisibility(View.INVISIBLE);
            tv_enterOverall.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
        }else if (et_tallage.getText().toString().equals("")) {
            tv_netProceeds.setText("");
            tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
            tv_enterSalary.setVisibility(View.INVISIBLE);
            tv_enterLease.setVisibility(View.INVISIBLE);
            tv_enterTallage.setVisibility(View.VISIBLE);
            tv_enterOverall.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
        }else if (et_salary.getText().toString().equals("")) {
            tv_netProceeds.setText("");
            tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
            tv_enterSalary.setVisibility(View.VISIBLE);
            tv_enterLease.setVisibility(View.INVISIBLE);
            tv_enterTallage.setVisibility(View.INVISIBLE);
            tv_enterOverall.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
        }  else if (et_overall.getText().toString().equals("")) {
            tv_netProceeds.setText("");
            tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
            tv_enterSalary.setVisibility(View.INVISIBLE);
            tv_enterLease.setVisibility(View.INVISIBLE);
            tv_enterTallage.setVisibility(View.INVISIBLE);
            tv_enterOverall.setVisibility(View.VISIBLE);
            tv_enterMarkup.setVisibility(View.INVISIBLE);
        } else if (et_markup.getText().toString().equals("")) {
            tv_netProceeds.setText("");
            tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
            tv_enterSalary.setVisibility(View.INVISIBLE);
            tv_enterLease.setVisibility(View.INVISIBLE);
            tv_enterTallage.setVisibility(View.INVISIBLE);
            tv_enterOverall.setVisibility(View.INVISIBLE);
            tv_enterMarkup.setVisibility(View.VISIBLE);
        }

        try {
            saleProceeds = Double.parseDouble(et_saleProceeds.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            salary = Double.parseDouble(et_salary.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            lease = Double.parseDouble(et_lease.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            tallage = Double.parseDouble(et_tallage.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            overall = Double.parseDouble(et_overall.getText().toString());
        } catch (Exception e) {
            return;
        }
        try {
            markup = Double.parseDouble(et_markup.getText().toString());
        } catch (Exception e) {
            return;
        }


        temp = (markup / 100) + 1;
        percent = (saleProceeds / temp);

        result = saleProceeds - percent - salary - lease - tallage - overall;

        tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
        tv_enterSalary.setVisibility(View.INVISIBLE);
        tv_enterLease.setVisibility(View.INVISIBLE);
        tv_enterTallage.setVisibility(View.INVISIBLE);
        tv_enterOverall.setVisibility(View.INVISIBLE);
        tv_enterMarkup.setVisibility(View.INVISIBLE);
        tv_netProceeds.setText("" + (int) result);

    }

    private TextWatcher et_leaseWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateNetProceeds();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextWatcher et_tallageWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateNetProceeds();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextWatcher et_overallWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateNetProceeds();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextWatcher et_markupWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            updateNetProceeds();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextWatcher et_salaryWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateNetProceeds();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextWatcher et_saleProceedsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateNetProceeds();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public void buttonClick(View view) {

        switch (view.getId()) {
            case R.id.a3_btnClear:
                if (et_tallage==getCurrentFocus()){
                    et_tallage.setText("");
                    tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
                    tv_enterLease.setVisibility(View.INVISIBLE);
                    tv_enterMarkup.setVisibility(View.INVISIBLE);
                    tv_enterOverall.setVisibility(View.INVISIBLE);
                    tv_enterSalary.setVisibility(View.INVISIBLE);
                    tv_enterTallage.setVisibility(View.VISIBLE);
                }
                if (et_markup==getCurrentFocus()){
                    et_markup.setText("");
                    tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
                    tv_enterLease.setVisibility(View.INVISIBLE);
                    tv_enterMarkup.setVisibility(View.VISIBLE);
                    tv_enterOverall.setVisibility(View.INVISIBLE);
                    tv_enterSalary.setVisibility(View.INVISIBLE);
                    tv_enterTallage.setVisibility(View.INVISIBLE);
                }
                if (et_overall==getCurrentFocus()){
                    et_overall.setText("");
                    tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
                    tv_enterLease.setVisibility(View.INVISIBLE);
                    tv_enterMarkup.setVisibility(View.INVISIBLE);
                    tv_enterOverall.setVisibility(View.VISIBLE);
                    tv_enterSalary.setVisibility(View.INVISIBLE);
                    tv_enterTallage.setVisibility(View.INVISIBLE);
                }
                if (et_lease==getCurrentFocus()){
                    et_lease.setText("");
                    tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
                    tv_enterLease.setVisibility(View.VISIBLE);
                    tv_enterMarkup.setVisibility(View.INVISIBLE);
                    tv_enterOverall.setVisibility(View.INVISIBLE);
                    tv_enterSalary.setVisibility(View.INVISIBLE);
                    tv_enterTallage.setVisibility(View.INVISIBLE);
                }
                if (et_salary==getCurrentFocus()){
                    et_salary.setText("");
                    tv_enterSaleProceeds.setVisibility(View.INVISIBLE);
                    tv_enterLease.setVisibility(View.INVISIBLE);
                    tv_enterMarkup.setVisibility(View.INVISIBLE);
                    tv_enterOverall.setVisibility(View.INVISIBLE);
                    tv_enterSalary.setVisibility(View.VISIBLE);
                    tv_enterTallage.setVisibility(View.INVISIBLE);
                }
                if (et_saleProceeds==getCurrentFocus()){
                    et_saleProceeds.setText("");
                    tv_enterSaleProceeds.setVisibility(View.VISIBLE);
                    tv_enterLease.setVisibility(View.INVISIBLE);
                    tv_enterMarkup.setVisibility(View.INVISIBLE);
                    tv_enterOverall.setVisibility(View.INVISIBLE);
                    tv_enterSalary.setVisibility(View.INVISIBLE);
                    tv_enterTallage.setVisibility(View.INVISIBLE);
                }

                break;
            case R.id.a3_btnEnter:

                if (et_saleProceeds==getCurrentFocus()&&focusable){
                    et_lease.requestFocus();
                    focusable=false;
                }
                if (et_lease==getCurrentFocus()&&focusable){
                    et_tallage.requestFocus();
                    focusable=false;
                }
                if (et_tallage==getCurrentFocus()&&focusable){
                    et_salary.requestFocus();
                    focusable=false;
                }
                if (et_salary==getCurrentFocus()&&focusable){
                    et_overall.requestFocus();
                    focusable=false;
                }
                if (et_overall==getCurrentFocus()&&focusable){
                    et_markup.requestFocus();
                    focusable=false;
                }
                if (et_markup==getCurrentFocus()&&focusable){
                    et_saleProceeds.requestFocus();
                    focusable=false;
                }
                break;

            case R.id.a3_btnPoint:
                if (et_saleProceeds==getCurrentFocus()){
                    et_saleProceeds.setText(et_saleProceeds.getText()+".");
                }
                if (et_lease==getCurrentFocus()){
                    et_lease.setText(et_lease.getText()+".");
                }
                if (et_tallage==getCurrentFocus()){
                    et_tallage.setText(et_tallage.getText()+".");
                }
                if (et_salary==getCurrentFocus()){
                    et_salary.setText(et_salary.getText()+".");
                }
                if (et_overall==getCurrentFocus()){
                    et_overall.setText(et_overall.getText()+".");
                }
                if (et_markup==getCurrentFocus()){
                    et_markup.setText(et_markup.getText()+".");
                }
                break;
            default:{
                if (et_saleProceeds==getCurrentFocus()){
                    et_saleProceeds.setText(et_saleProceeds.getText()+view.getContentDescription().toString());
                    focusable=true;
                }
                if (et_lease==getCurrentFocus()){
                    et_lease.setText(et_lease.getText()+view.getContentDescription().toString());
                    focusable=true;
                }
                if (et_tallage==getCurrentFocus()){
                    et_tallage.setText(et_tallage.getText()+view.getContentDescription().toString());
                    focusable=true;
                }
                if (et_salary==getCurrentFocus()){
                    et_salary.setText(et_salary.getText()+view.getContentDescription().toString());
                    focusable=true;
                }
                if (et_overall==getCurrentFocus()){
                    et_overall.setText(et_overall.getText()+view.getContentDescription().toString());
                    focusable=true;
                }
                if (et_markup==getCurrentFocus()){
                    et_markup.setText(et_markup.getText()+view.getContentDescription().toString());
                    focusable=true;
                }
            }
            break;
        }

    }

    private void showToastMessage(String message) {
        Toast toast=Toast.makeText(this,message,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM,0,0);
        toast.show();
    }
}
